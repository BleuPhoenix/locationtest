package com.bleuphoenix.locationtest.listeners;

/**
 * Created by Jemsheer K D on 24 February, 2020.
 * Package com.example.locationtest.listeners
 * Project LocationTest
 */

public interface PermissionListener {

    void onPermissionCheckCompleted(int requestCode, boolean isPermissionGranted);

}