package com.bleuphoenix.locationtest.ui.main

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.bleuphoenix.locationtest.R
import com.bleuphoenix.locationtest.constants.CCConstants
import com.bleuphoenix.locationtest.databinding.ActivityMainBinding
import com.bleuphoenix.locationtest.listeners.PermissionListener
import com.bleuphoenix.locationtest.services.FusedListenerService
import com.bleuphoenix.locationtest.services.GeofenceService
import com.bleuphoenix.locationtest.services.LocationListenerService
import com.bleuphoenix.locationtest.ui.core.BaseActivity
import com.bleuphoenix.locationtest.ui.main.MainHandler.MainHandlerListener
import com.bleuphoenix.locationtest.utils.CCCommonUtils

class MainActivity : BaseActivity() {

    companion object {
        private const val TAG = "MainA"
    }

    private lateinit var handler: MainHandler
    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        binding.viewModel = viewModel

        handler = MainHandler(object : MainHandlerListener {
            override fun onFusedStartServiceClick(view: View) {
                viewModel.selectedLocationProviderType = MainViewModel.FUSED_LOCATION_CLIENT
                if (checkForLocationPermissions()) {
                    performFusedStartService()
                } else {
                    getLocationPermissions()
                }
            }

            override fun onFusedStopServiceClick(view: View) {
                viewModel.selectedLocationProviderType = MainViewModel.NONE
                performFusedStopService()
            }

            override fun onLocationManagerStartServiceClick(view: View) {
                viewModel.selectedLocationProviderType = MainViewModel.LOCATION_LISTENER
                if (checkForLocationPermissions()) {
                    performLocationListenerStartService()
                } else {
                    getLocationPermissions()
                }
            }

            override fun onLocationManagerStopServiceClick(view: View) {
                viewModel.selectedLocationProviderType = MainViewModel.NONE
                performLocationListenerStopService()
            }

            override fun onGeofencingStartServiceClick(view: View) {
                viewModel.selectedLocationProviderType = MainViewModel.GEOFENCING
                if (checkForLocationPermissions()) {
                    performGeofencingStartService()
                } else {
                    getLocationPermissions()
                }
            }

            override fun onGeofencingStopServiceClick(view: View) {
                viewModel.selectedLocationProviderType = MainViewModel.NONE
                performGeofencingStopService()
            }
        })
        binding.handler = handler


        val permissionListener = PermissionListener { requestCode, isPermissionGranted ->
            if (requestCode == REQUEST_PERMISSIONS_LOCATION) {
                if (isPermissionGranted) {
                    performFusedStartService()
                } else {
                    Log.i(TAG, "onPermissionCheckCompleted: Permission Denied")
                }
            }
        }
        addPermissionListener(permissionListener)
    }

    private fun performFusedStartService() {
        val intent = Intent(this, FusedListenerService::class.java)
        intent.putExtra(CCConstants.SDK_STARTWITH_LOCATION_UPDATE, true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent)
        } else {
            startService(intent)
        }
    }

    private fun performFusedStopService() {
        viewModel.isProgressVisible.set(true)
        val intent = Intent(this, FusedListenerService::class.java)
        if (CCCommonUtils.isLocationServiceRunning(this, FusedListenerService::class.java)) {
            stopService(intent)
        }
        Handler().postDelayed({ viewModel.isProgressVisible.set(false) }, 2000)
    }

    private fun performLocationListenerStartService() {
        val intent = Intent(this, LocationListenerService::class.java)
        intent.putExtra(CCConstants.SDK_STARTWITH_LOCATION_UPDATE, true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent)
        } else {
            startService(intent)
        }
    }

    private fun performLocationListenerStopService() {
        viewModel.isProgressVisible.set(true)
        val intent = Intent(this, LocationListenerService::class.java)
        if (CCCommonUtils.isLocationServiceRunning(this, LocationListenerService::class.java)) {
            stopService(intent)
        }
        Handler().postDelayed({ viewModel.isProgressVisible.set(false) }, 2000)
    }

    private fun performGeofencingStartService() {
        val intent = Intent(this, GeofenceService::class.java)
        intent.putExtra(CCConstants.SDK_STARTWITH_LOCATION_UPDATE, true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent)
        } else {
            startService(intent)
        }
    }

    private fun performGeofencingStopService() {
        viewModel.isProgressVisible.set(true)
        val intent = Intent(this, GeofenceService::class.java)
        if (CCCommonUtils.isLocationServiceRunning(this, GeofenceService::class.java)) {
            stopService(intent)
        }
        Handler().postDelayed({ viewModel.isProgressVisible.set(false) }, 2000)
    }


}