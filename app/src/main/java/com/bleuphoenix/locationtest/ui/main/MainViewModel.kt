package com.bleuphoenix.locationtest.ui.main

import androidx.databinding.ObservableBoolean
import com.thinkpalm.pixabay.ui.core.BaseViewModel

/**
 * Created by Jemsheer K D on 24 February, 2020.
 * Package com.example.locationtest.ui.main
 * Project LocationTest
 */
class MainViewModel : BaseViewModel() {

    companion object {
        const val NONE = -1
        const val FUSED_LOCATION_CLIENT = 0
        const val LOCATION_LISTENER = 1
        const val GEOFENCING = 2
    }

    var isProgressVisible = ObservableBoolean(false)
    var selectedLocationProviderType = NONE

}