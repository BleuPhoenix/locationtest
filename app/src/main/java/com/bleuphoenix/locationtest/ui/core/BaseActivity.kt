package com.bleuphoenix.locationtest.ui.core

import android.Manifest
import android.content.pm.PackageManager
import android.content.res.Resources
import android.content.res.TypedArray
import android.os.Build
import android.util.TypedValue
import android.view.HapticFeedbackConstants
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.bleuphoenix.locationtest.R
import com.bleuphoenix.locationtest.listeners.PermissionListener
import com.bleuphoenix.locationtest.models.core.MessageBean
import com.google.android.material.snackbar.Snackbar


/**
 * Created by Jemsheer K D on 17 August, 2019.
 * Package com.thinkpalm.pixabay.ui.core
 * Project Pixabay
 */
abstract class BaseActivity : AppCompatActivity() {


    companion object {
        const val REQUEST_ENABLE_BT = 0
        const val REQUEST_PERMISSIONS = 1
        const val REQUEST_PERMISSIONS_LOCATION = 2
        const val REQUEST_PERMISSIONS_READ_CONTACTS = 3
        const val REQUEST_PERMISSIONS_READ_WRITE = 4
        const val REQUEST_PERMISSIONS_GET_ACCOUNTS = 5
        const val REQUEST_PERMISSIONS_READ_PHONE_STATE = 6
        const val REQUEST_PERMISSIONS_CALL = 7
        const val REQUEST_PERMISSIONS_SMS = 8
        private const val TAG = "BaseA"

        protected var px = 0f
        protected var isBluetoothEnableRequestShown = false
        protected var width = 0
        protected var height = 0
        protected var selectableItemBackground: Int = 0
    }


    private var hasSMSPermissions: Boolean = true
    private var hasCallPermissions: Boolean = true
    private var hasReadPhoneStatePermissions: Boolean = true
    private var hasGetAccountsPermissions: Boolean = true
    private var hasReadWritePermissions: Boolean = true
    private var hasLocationPermissions: Boolean = true
    private var hasReadContactsPermissions: Boolean = true
    private var hasAllPermissions: Boolean = true
    private lateinit var r: Resources
    private var mActionBarHeight = 0f

    private var permissionListeners = arrayListOf<PermissionListener>()


    protected fun initBase() {

        val mstyled: TypedArray = theme.obtainStyledAttributes(intArrayOf(android.R.attr.actionBarSize))
        mActionBarHeight = mstyled.getDimension(0, 0f)
        mstyled.recycle()


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        }
/*        if (android.os.Build.VERSION.SDK_INT >= 21) {
            toolbar.setPadding(0, getStatusBarHeight(), 0, 0);
        }*/

        //	llBottomBarActionPopup=(LinearLayout)findViewById(R.id.ll_bottombar_popmenu);

        val typedValue = TypedValue()
        theme.resolveAttribute(R.attr.selectableItemBackground, typedValue, true)
        selectableItemBackground = typedValue.resourceId


        r = resources
        px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1f, r.displayMetrics)
        width = r.displayMetrics.widthPixels
        height = r.displayMetrics.heightPixels


    }


    private val snackBarDismissOnClickListener: View.OnClickListener =
            View.OnClickListener { view ->
                view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                //mVibrator.vibrate(25);
                view.visibility = View.GONE
            }

    fun setProgressVisibilityStatus(progressVisibility: LiveData<Boolean>) {
        progressVisibility.observe(this, Observer<Boolean?> {
            it?.let {
                if (it) {

                } else {

                }
            }
        })
    }

    fun setMessageBox(view: View, message: LiveData<MessageBean>) {
        message.observe(this, Observer<MessageBean?> {
            it?.let {
                if (it.type == MessageBean.MESSAGE_TYPE_SNACK_BAR) {
                    Snackbar.make(view, it.message, it.time)
                            .setAction(R.string.btn_dismiss, snackBarDismissOnClickListener).show()
                } else {
                    Toast.makeText(applicationContext, it.message, it.time).show()
                }
            }
        })
    }

    // A method to find height of the status bar
    fun getStatusBarHeight(): Int {
        var result = 0
        val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            result = resources.getDimensionPixelSize(resourceId)
        }
        return result
    }

    fun getActionBarHeight(): Float {
        val mstyled = theme.obtainStyledAttributes(intArrayOf(android.R.attr.actionBarSize))
        val mActionBarHeight = mstyled.getDimension(0, 0f)
        mstyled.recycle()
        return mActionBarHeight
    }


    fun addPermissionListener(permissionListener: PermissionListener) {
        permissionListeners.add(permissionListener)
    }

    protected fun checkForPermissions(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            hasAllPermissions = !(ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.CLEAR_APP_CACHE) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED)
        } else {
            hasAllPermissions = true
        }
        return hasAllPermissions
    }

    protected fun getAllPermssions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.CLEAR_APP_CACHE) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                val permissions = arrayOf(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.READ_CONTACTS,
                        Manifest.permission.GET_ACCOUNTS,
                        Manifest.permission.CLEAR_APP_CACHE,
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.INTERNET,
                        Manifest.permission.READ_PHONE_STATE
                )

                ActivityCompat.requestPermissions(this, permissions, REQUEST_PERMISSIONS)
            }
        }
    }

    protected fun checkForContactsPermissions(): Boolean {
        hasReadContactsPermissions = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
        } else {
            true
        }
        return hasReadContactsPermissions
    }

    protected fun getContactsPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

                val permissions = arrayOf(
                        Manifest.permission.READ_CONTACTS
                )
                ActivityCompat.requestPermissions(this, permissions, REQUEST_PERMISSIONS_READ_CONTACTS)
            }
        }
    }

    protected fun checkForLocationPermissions(): Boolean {
        hasLocationPermissions = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            !(ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        } else {
            true
        }
        return hasLocationPermissions
    }

    protected fun getLocationPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                val permissions = arrayOf(
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                )
                ActivityCompat.requestPermissions(this, permissions, REQUEST_PERMISSIONS_LOCATION)
            }
        }
    }

    protected fun checkForReadWritePermissions(): Boolean {
        hasReadWritePermissions = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            !(ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        } else {
            true
        }
        return hasReadWritePermissions
    }

    protected fun getReadWritePermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                val permissions = arrayOf(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                ActivityCompat.requestPermissions(this, permissions, REQUEST_PERMISSIONS_READ_WRITE)
            }
        }
    }

    protected fun checkForGetAccountsPermissions(): Boolean {
        hasGetAccountsPermissions = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.GET_ACCOUNTS) == PackageManager.PERMISSION_GRANTED
        } else {
            true
        }
        return hasGetAccountsPermissions
    }

    protected fun getGetAccountsPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
                val permissions = arrayOf(Manifest.permission.GET_ACCOUNTS)
                ActivityCompat.requestPermissions(this, permissions, REQUEST_PERMISSIONS_GET_ACCOUNTS)
            }
        }
    }


    protected fun checkForReadPhoneStatePermissions(): Boolean {
        hasReadPhoneStatePermissions = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
        } else {
            true
        }
        return hasReadPhoneStatePermissions
    }

    protected fun getReadPhoneStatePermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                val permissions = arrayOf(Manifest.permission.READ_PHONE_STATE)
                ActivityCompat.requestPermissions(this, permissions, REQUEST_PERMISSIONS_READ_PHONE_STATE)
            }
        }
    }

    protected fun checkForCallPermissions(): Boolean {
        hasCallPermissions = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED
        } else {
            true
        }
        return hasCallPermissions
    }

    protected fun getCallPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                val permissions = arrayOf(Manifest.permission.CALL_PHONE)
                ActivityCompat.requestPermissions(this, permissions, REQUEST_PERMISSIONS_CALL)
            }
        }
    }

    protected fun checkForSMSPermissions(): Boolean {
        hasSMSPermissions = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED
        } else {
            true
        }
        return hasSMSPermissions;
    }

    protected fun getSMSPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                val permissions = arrayOf(Manifest.permission.SEND_SMS)
                ActivityCompat.requestPermissions(this, permissions, REQUEST_PERMISSIONS_SMS)
            }
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_PERMISSIONS) {
            hasAllPermissions = grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED
                    && grantResults[3] == PackageManager.PERMISSION_GRANTED
                    && grantResults[4] == PackageManager.PERMISSION_GRANTED
                    && grantResults[5] == PackageManager.PERMISSION_GRANTED
                    && grantResults[6] == PackageManager.PERMISSION_GRANTED
                    && grantResults[7] == PackageManager.PERMISSION_GRANTED
                    && grantResults[8] == PackageManager.PERMISSION_GRANTED
                    && grantResults[9] == PackageManager.PERMISSION_GRANTED
            try {
                setPermissionCheckStatus(requestCode, hasAllPermissions)
            } catch (ignored: Exception) {
            }
        }
        if (requestCode == REQUEST_PERMISSIONS_LOCATION) {
            if (grantResults.size == 2) {
                hasLocationPermissions = grantResults[1] == PackageManager.PERMISSION_GRANTED
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                try {
                    setPermissionCheckStatus(requestCode, hasLocationPermissions)
                } catch (ignored: Exception) {
                }
            }
        }
        if (requestCode == REQUEST_PERMISSIONS_READ_WRITE) {
            if (grantResults.size == 2) {
                hasReadWritePermissions = grantResults[1] == PackageManager.PERMISSION_GRANTED
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                try {
                    setPermissionCheckStatus(requestCode, hasReadWritePermissions)
                } catch (ignored: Exception) {
                }
            }
        }
        if (requestCode == REQUEST_PERMISSIONS_READ_CONTACTS) {
            if (grantResults.size == 1) {
                hasReadContactsPermissions = grantResults[0] == PackageManager.PERMISSION_GRANTED
                try {
                    setPermissionCheckStatus(requestCode, hasReadContactsPermissions)
                } catch (ignored: Exception) {
                }
            }
        }
        if (requestCode == REQUEST_PERMISSIONS_GET_ACCOUNTS) {
            if (grantResults.size == 1) {
                hasGetAccountsPermissions = grantResults[0] == PackageManager.PERMISSION_GRANTED
                try {
                    setPermissionCheckStatus(requestCode, hasGetAccountsPermissions)
                } catch (ignored: Exception) {
                }
            }
        }


        if (requestCode == REQUEST_PERMISSIONS_READ_PHONE_STATE) {
            if (grantResults.size == 1) {
                hasReadPhoneStatePermissions = grantResults[0] == PackageManager.PERMISSION_GRANTED
                try {
                    setPermissionCheckStatus(requestCode, hasReadPhoneStatePermissions)
                } catch (ignored: Exception) {
                }
            }
        }

        if (requestCode == REQUEST_PERMISSIONS_CALL) {
            if (grantResults.size == 1) {
                hasCallPermissions = grantResults[0] == PackageManager.PERMISSION_GRANTED
                try {
                    setPermissionCheckStatus(requestCode, hasCallPermissions)
                } catch (ignored: Exception) {
                }
            }
        }
        if (requestCode == REQUEST_PERMISSIONS_SMS) {
            if (grantResults.size == 1) {
                hasSMSPermissions = grantResults[0] == PackageManager.PERMISSION_GRANTED
                try {
                    setPermissionCheckStatus(requestCode, hasSMSPermissions)
                } catch (ignored: Exception) {
                }
            }
        }

    }

    private fun setPermissionCheckStatus(requestCode: Int, hasPermission: Boolean) {
        for (permissionListener in permissionListeners) {
            permissionListener.onPermissionCheckCompleted(requestCode, hasPermission)
        }
    }
}
