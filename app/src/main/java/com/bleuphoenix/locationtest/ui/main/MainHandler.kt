package com.bleuphoenix.locationtest.ui.main

import android.view.HapticFeedbackConstants
import android.view.View

/**
 * Created by Jemsheer K D on 24 February, 2020.
 * Package com.example.locationtest.ui.main
 * Project LocationTest
 */
class MainHandler(private val listener: MainHandlerListener) {
    fun onFusedStartServiceClick(view: View) {
        view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
        listener.onFusedStartServiceClick(view)
    }

    fun onFusedStopServiceClick(view: View) {
        view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
        listener.onFusedStopServiceClick(view)
    }

    fun onLocationManagerStartServiceClick(view: View) {
        view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
        listener.onLocationManagerStartServiceClick(view)
    }

    fun onLocationManagerStopServiceClick(view: View) {
        view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
        listener.onLocationManagerStopServiceClick(view)
    }
    fun onGeofencingStartServiceClick(view: View) {
        view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
        listener.onGeofencingStartServiceClick(view)
    }

    fun onGeofencingStopServiceClick(view: View) {
        view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
        listener.onGeofencingStopServiceClick(view)
    }

    interface MainHandlerListener {
        fun onFusedStartServiceClick(view: View)
        fun onFusedStopServiceClick(view: View)
        fun onLocationManagerStartServiceClick(view: View)
        fun onLocationManagerStopServiceClick(view: View)
        fun onGeofencingStartServiceClick(view: View)
        fun onGeofencingStopServiceClick(view: View)
    }

}