package com.bleuphoenix.locationtest.models;


/**
 * Class represents response object of SET_USER_LOCATION api
 */
public class CCSetUserLocationResponse {
    private final double minimumDistance;
    private final int maximumTime;
    private final int MAXIMUM_TIME_NOT_AVAILABLE = -1;

    public CCSetUserLocationResponse(double minimumDistance) {
        this.minimumDistance = minimumDistance;
        this.maximumTime = MAXIMUM_TIME_NOT_AVAILABLE;
    }

    public CCSetUserLocationResponse(double minimumDistance, int maximumTime) {
        this.minimumDistance = minimumDistance;
        this.maximumTime = maximumTime;
    }

    /**
     * Minimum distance after which SDK should look for location change.
     *
     * @return double value having minimum distance
     */
    public double getMinimumDistance() {
        return minimumDistance;
    }

    /**
     * This is optionally populated, @see isMaximumTimeProvided to see if this
     * value is provided
     *
     * @return maximum time as an integer if provided or -1 if its not provided
     */
    public int getMaximumTime() {
        return maximumTime;
    }

    /**
     * @return <em>true</em> : If the optional parameter <em>maximum time</em> is provided , returns <em>false</em> otherwise
     */
    public boolean isMaximumTimeProvided() {
        return maximumTime != MAXIMUM_TIME_NOT_AVAILABLE;
    }
}
