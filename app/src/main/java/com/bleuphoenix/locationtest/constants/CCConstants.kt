package com.bleuphoenix.locationtest.constants

object CCConstants {
    const val MIN_ACCURACY = 8046
    const val PROXIMITY_RANGE = 1806
    const val LOCATION_UPDATE_INTERVAL_DIFF = 10 * 60 //10 min

    //Follow me variables...
    const val LOCATION_UPDATE_SCHEDULAR = 1000 * 60 * 30 //60 minute
    const val LOCATION_DEFAULT_TIME = 1000 * 60 * 59 //59 minute //Note - Default value from server is 172800 which 2.8 minute
    const val LOCATION_FASTEST_UPDATE_TIME = 1000 * 60 * 30 //30 minute
    const val LOCATION_DEFAULT_DISTANCE = 1000f // 1km
    const val SDK_STARTWITH_LOCATION_UPDATE = "startWithLocationUpdate"
    const val NOTIFICATION_CONTENT_TITLE = "App is running in background"

    /*BroadCast Intent Action*/
    const val ACTION_GEOFENCE_EXIT_RECEIVED = "ACTION_GEOFENCE_EXIT_RECEIVED"

    /*BroadCast Intent Extras*/
    const val EXTRA_LOCATION = "extra_geofence_location"

    /*Custom Permission For BroadCast*/
    const val PERMISSION_BROADCAST = ".permission.BROADCAST"
}