package com.bleuphoenix.locationtest.services;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.bleuphoenix.locationtest.constants.CCConstants;
import com.bleuphoenix.locationtest.models.CCSetUserLocationResponse;
import com.bleuphoenix.locationtest.utils.CCCommonUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

/**
 * Created by Jemsheer K D on 24 February, 2020.
 * Package com.example.locationtest.services
 * Project LocationTest
 */
public class FusedListenerService extends Service implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener {


    private static final String TAG = "FusedLocationS";
    private long lastUpdateTime = 0;
    private Location lastUpdatedLocation;


    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;

    private boolean sendQuickUpdate = false;

    private static final int NOTIFICATION_ID = 1234567;

    private static final String NOTIFICATION_CHANNEL_NAME = "FusedLocationService";

    String CHANNEL_ID = "channel_101";

    /*Fused Location Provider Client & Fused Location Callback*/
    private LocationCallback locationCallback;
    private FusedLocationProviderClient fusedLocationClient;
    private float minimumDistance = CCConstants.LOCATION_DEFAULT_DISTANCE;

    @Override
    public void onCreate() {
        if (CCCommonUtils.checkAppPermission(getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            startFusedTracking();

            initFusedLocationCallback();

        }
        // handleForegroundNotification();
    }

    private void initFusedLocationCallback() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        /*Location Callback for Fused Location API*/
        locationCallback = new LocationCallback() {

            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    processLocationUpdate(location);
                }
            }
        };
    }

    private void processLocationUpdate(Location location) {
        long currentTime = System.currentTimeMillis();
        Log.i("CCLocationService", "processLocationUpdate: Location : "
                + location.getLatitude() + ", " + location.getLongitude());
        if (/*(lastUpdateTime == 0 || (currentTime - lastUpdateTime) > 60000)
                ||*/ (lastUpdatedLocation == null || lastUpdatedLocation.distanceTo(location) > minimumDistance)) {
            if (lastUpdatedLocation != null) {
                Log.i(TAG, "processLocationUpdate: LastUpdatedLocation : "
                        + lastUpdatedLocation.getLatitude() + ", " + lastUpdatedLocation.getLongitude());
                Log.i(TAG, "processLocationUpdate: DistanceTo : " + lastUpdatedLocation.distanceTo(location));

            }
            lastUpdatedLocation = location;
            postData(location, false, "*Fused Location Result*");
            Log.i("FusedLocationS", "Location Update From Fused Location" +
                    "\nLocation : " + location.getLatitude() + ", " + location.getLongitude());
        }
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Log.d("CCLocationService::onStartCommand",
        // "Location service started on: " + new Date());
        //CHANNEL_ID = String.valueOf(getRandomNumber());

        if (CCCommonUtils.checkAppPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)) {
            registerLocationManager(false, CCConstants.LOCATION_DEFAULT_TIME,
                    CCConstants.LOCATION_DEFAULT_DISTANCE);
        }
        // When user turn on location control, send immediate update to server.
        if (intent != null) {
            sendQuickUpdate = intent.getBooleanExtra(CCConstants.SDK_STARTWITH_LOCATION_UPDATE, true);
            if (fusedLocationClient != null) {
                if (CCCommonUtils.checkAppPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                    refreshLastLocation();
                }
                if (lastUpdatedLocation != null && sendQuickUpdate) {
                    postData(lastUpdatedLocation, false, "*Service Start*");
                } else {
                    // Log.d("CCLocationService::onStartCommand",
                    // "** Last known location not found found at service start. **");
                }
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startCustomForeground();
        } else {
            startForeground(NOTIFICATION_ID, getNotification());
        }
        return Service.START_STICKY;
    }

    /**
     * Custom start foreground method for API Level O and above
     * Content Title is derived from Citi Location SDK
     * Dialog info icon is derived from Citi Location SDK
     * Applying existing channel_1061 CHANNEL_ID for the custom foreground method as well
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void startCustomForeground() {
        String NOTIFICATION_CHANNEL_ID = CHANNEL_ID;
        String channelName = NOTIFICATION_CHANNEL_NAME;
        NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
        notificationChannel.setLightColor(Color.BLUE);
        notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert manager != null;
        manager.createNotificationChannel(notificationChannel);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        Notification notification = notificationBuilder.setOngoing(true)
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .setContentTitle(CCConstants.NOTIFICATION_CONTENT_TITLE)
                .setPriority(NotificationManager.IMPORTANCE_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build();
        startForeground(NOTIFICATION_ID, notification);
    }

    private void startForegroundNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            startForeground();
        else {
            startForeground(NOTIFICATION_ID, getNotification());
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private void startForeground() {
        NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, getApplicationName(this), NotificationManager.IMPORTANCE_NONE);
        notificationChannel.setLightColor(Color.BLUE);
        notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null)
            notificationManager.createNotificationChannel(notificationChannel);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);
        Notification notification = notificationBuilder.setOngoing(true)
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .setContentTitle("Service running")
                .setPriority(NotificationManager.IMPORTANCE_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build();
        startForeground(NOTIFICATION_ID, notification);
    }

    /**
     * Build notification for showing, when long running task is in background.
     * Notification will be initiated as foreground service.
     *
     * @return notification
     */
    private Notification getNotification() {
        @SuppressWarnings("deprecation")
        Notification.Builder builder = new Notification.Builder(this)
                .setOngoing(true).setContentText("Service running")
                .setPriority(Notification.PRIORITY_DEFAULT)
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .setWhen(System.currentTimeMillis());
        return builder.build();
    }

    public static String getApplicationName(Context context) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        int stringId = applicationInfo.labelRes;
        return stringId == 0 ? applicationInfo.nonLocalizedLabel.toString() : context.getString(stringId);
    }

    private void refreshLastLocation() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (locationManager != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null) {
                                // Logic to handle location object
                                processLocationUpdate(location);
                            }
                        }
                    });
        }
    }

    public void registerLocationManager(boolean isFromResponse, int timeInterval, double distance) {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        String provider = locationManager.getBestProvider(criteria, true);
        if (provider != null) {
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            Log.i(TAG, "registerLocationManager: From API Response : Time Interval : "
                    + timeInterval + ", Distance : " + distance);
            minimumDistance = (float) distance;
//            updateFusedLocationRequest(timeInterval, (float) distance);
            updateFusedLocationRequest(30000, minimumDistance);

        }
    }

    private void updateFusedLocationRequest(int timeInterval, float distance) {
        if (locationRequest == null) {
            locationRequest = LocationRequest.create();
        }
        locationRequest.setInterval(timeInterval);
        locationRequest.setFastestInterval(10000);
        locationRequest.setSmallestDisplacement(distance);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
    }

    public void postData(Location location, boolean isPostingOldLocation, String from) {
        if (location == null) {
            // Log.d("CCLocationService::postData", " ** No location found when posting data on server from "+from);
            return;
        }

        long currentTime = System.currentTimeMillis();
        if (lastUpdateTime == 0 || (currentTime - lastUpdateTime) > 60000) { // 1minute
            lastUpdateTime = System.currentTimeMillis();

            HashMap<String, Object> fieldData = new HashMap<String, Object>();
            double accuracy = location.getAccuracy();
            if (accuracy > CCConstants.MIN_ACCURACY) {
                accuracy = CCConstants.MIN_ACCURACY;
            }
            fieldData.put("latitude", location.getLatitude());
            fieldData.put("longitude", location.getLongitude());
            fieldData.put("accuracy", accuracy);
            String countryCode = "US";
            countryCode = getCountryCode(location.getLatitude(), location.getLongitude());
            fieldData.put("countryCode", countryCode);
            Log.d("location update", "Time: " + getDate() + "\nLocation: " + location.getLatitude() + ", " + location.getLongitude() + "");
        }
    }


    public void reconfigureLocationManager(CCSetUserLocationResponse locationResponse) {
        if (locationResponse == null) {
            Log.e("FusedLocationS", "Empty locationReponse object passed to reconfigureLocationManager");
            return;
        }
        if (!CCCommonUtils.checkAppPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)) {
            return;
        }
        double distance = locationResponse.getMinimumDistance();
        int time = locationResponse.getMaximumTime();
        if (distance != 0.0 || time != 0) {
            if (distance != 0.0) {
                registerLocationManager(true, 30000, distance);
            } else {
                registerLocationManager(true, 30000, CCConstants.PROXIMITY_RANGE);
            }
        } else {
            registerLocationManager(true, CCConstants.LOCATION_DEFAULT_TIME,
                    CCConstants.LOCATION_DEFAULT_DISTANCE);
        }
    }

    @Override
    public void onDestroy() {
        if (fusedLocationClient != null) {
            fusedLocationClient.removeLocationUpdates(locationCallback);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Observed in few scenarios onDestroy() callback was not triggered after context.stopService(....)
     * So the below method has been implemented to stop the foreground service explicitly
     * StopForeground -> true will remove the foreground notification
     */
    public void stopForegroundService() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            stopSelf();
            stopForeground(true);
            onDestroy();
        } else {
            stopForeground(true);
            stopSelf();
            onDestroy();
        }

    }

    /**
     * In specific network conditions GeoCoder API throws grpc failed exception.
     * So created TelephonyManager based logic as a fallback mechanism
     *
     * @param latitude
     * @param longitude
     * @return
     */

    private String getCountryCode(double latitude, double longitude) {
        String countryCode = "US";
        Geocoder gCoder = new Geocoder(getApplicationContext());
        List<Address> addressList;
        try {
            addressList = gCoder.getFromLocation(latitude, longitude, 1);
            if (addressList != null && addressList.size() > 0) {
                countryCode = addressList.get(0).getCountryCode();
            }
        } catch (Exception e) {
            Log.e("getCountryCode", e.getMessage());
            TelephonyManager telephonyManager = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
            Log.d("ccLocationService", "getCountryCode fallback from Geocoder API to telephony manager" + telephonyManager.getNetworkCountryIso().toUpperCase());
            return telephonyManager.getNetworkCountryIso().toUpperCase();
        }
        return countryCode;
    }

    /**
     * Start fused google location api is google play service is available.
     */
    @SuppressWarnings("deprecation")
    private void startFusedTracking() {
        if (!CCCommonUtils.checkAppPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)) {
            return;
        }
        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();

            if (!googleApiClient.isConnected() || !googleApiClient.isConnecting()) {
                googleApiClient.connect();
            }
        } else {
            Log.e("startFusedTracking", "**unable to connect to google play services.**");
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onConnected(Bundle arg0) {
        // TODO Auto-generated method stub
        try {
            locationRequest = LocationRequest.create();
            locationRequest.setSmallestDisplacement(CCConstants.LOCATION_DEFAULT_DISTANCE);
//            locationRequest.setInterval(CCConstants.LOCATION_DEFAULT_TIME); // milliseconds
            locationRequest.setInterval(30000); // milliseconds
//            locationRequest.setFastestInterval(CCConstants.LOCATION_FASTEST_UPDATE_TIME);
            locationRequest.setFastestInterval(10000);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                return;
            }

            /*Fetching location using FusedLocationClient */
            fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
            updateFusedLocationRequest(10000, minimumDistance);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int arg0) {

    }

    @Override
    public void onLocationChanged(Location location) {
        postData(location, false, "*Fused API Listener*");
    }


    String getDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("MMM, dd yyyy HH:mm:ss");
        String newFormat = formatter.format(new Date());
        return newFormat;
    }
}
