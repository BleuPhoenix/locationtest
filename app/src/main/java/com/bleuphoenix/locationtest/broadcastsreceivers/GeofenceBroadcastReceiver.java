package com.bleuphoenix.locationtest.broadcastsreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.util.Log;

import com.bleuphoenix.locationtest.constants.CCConstants;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.google.android.gms.location.GeofencingEvent;

import java.util.List;

public class GeofenceBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = "GeofenceBrRec";

    public void onReceive(Context context, Intent intent) {
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        Location location = geofencingEvent.getTriggeringLocation();

        Log.i(TAG, "onReceive: GEOFENCING Location : "
                + location.getLatitude() + ", " + location.getLongitude());
        if (geofencingEvent.hasError()) {
            String errorMessage = GeofenceStatusCodes.getStatusCodeString(geofencingEvent.getErrorCode());
            Log.e(TAG, errorMessage);
            return;
        }

        // Get the transition type.
        int geofenceTransition = geofencingEvent.getGeofenceTransition();

        // Test that the reported transition was of interest.
        if (/*geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER ||*/
                geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {

            // Get the geofences that were triggered. A single event can trigger
            // multiple geofences.
            List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();

            Intent broadcastIntent = new Intent(CCConstants.ACTION_GEOFENCE_EXIT_RECEIVED);
            broadcastIntent.putExtra(CCConstants.EXTRA_LOCATION, geofencingEvent.getTriggeringLocation());
            context.sendBroadcast(broadcastIntent,
                    context.getPackageName() + CCConstants.PERMISSION_BROADCAST);

            // Get the transition details as a String.
            /*String geofenceTransitionDetails = getGeofenceTransitionDetails(
                    this,
                    geofenceTransition,
                    triggeringGeofences
            );*/

        }
    }
}
