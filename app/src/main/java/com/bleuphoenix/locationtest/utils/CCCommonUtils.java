package com.bleuphoenix.locationtest.utils;

/**
 * Created by Jemsheer K D on 24 February, 2020.
 * Package com.example.locationtest.utils
 * Project LocationTest
 */

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;


public class CCCommonUtils {
    private static volatile CCCommonUtils thisInstance;

    private CCCommonUtils() {
    }

    public static synchronized CCCommonUtils getInstance() {
        if (thisInstance == null) {
            thisInstance = new CCCommonUtils();
        }
        return thisInstance;
    }

    public static boolean isNullOrEmptyOrBlankString(String inputString) {
        boolean result = false;

        if (inputString == null || inputString.isEmpty()) {
            result = true;
        } else if (inputString.length() <= 0) {
            result = true;
        }
        return result;
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        }
        return false;
    }

    public static boolean isLocationEnabled(Context context) {
        final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            return false;
        }
        return true;
    }

    public static boolean isLocationPermissionAvailable(Context context) {
        if (!checkAppPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)) {
            return false;
        }
        return true;
    }

    public static boolean checkAppPermission(Context context, String permission) {
        if (Build.VERSION.SDK_INT >= 23) {
            int permissionStatus = context.checkSelfPermission(permission);
            if (permissionStatus != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public static void requestPermissionAccess(Context ctx, int requestCode, String... permissions) {
        if (permissions != null && permissions.length > 0) {
            if (ctx != null && ctx instanceof Activity)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    ((Activity) ctx).requestPermissions(permissions, requestCode);
                }
        }
    }

    public static void launchGPSOptions(Activity ctx) {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        final int REQUEST_CODE = 239;
        ctx.startActivityForResult(intent, REQUEST_CODE);
    }

    public static boolean isLocationServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static String getCountryCode(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getNetworkCountryIso();
    }

    public static String getCurrentNetworkName(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getNetworkOperatorName();
    }


}
